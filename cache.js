
/** 
 * Dummy caching class to simulate a more ideal caching solution such as distributed caching.
 */
module.exports = class DistributedCache {

    map = new Map();
    ttlMap = new Map();
    maxAgeMs;

    constructor(maxAgeSec = 60) {
        this.maxAgeMs = maxAgeSec * 1000;
    }

    isValid(ttl) {
        return ttl && (Date.now() - ttl) < this.maxAgeMs;
    }

    get(k) {
        const ttl = this.ttlMap.get(k);
        if (this.isValid(ttl)) {
            return this.map.get(k);
        }
        return undefined;
    }

    async set(k, v) {
        this.ttlMap.set(k, Date.now());
        return this.map.set(k, v);
    }
}