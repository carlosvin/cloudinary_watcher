
# Getting started

## Docker

```bash
docker run -e CLOUDINARY_URL="cloudinary://<public key>:<here the api_secret!>@<cloud name>" -p 49160:8080 -d carlosvin/cloudinary_watcher
```

After container is running we can just access to both endpoints at: 
 - Show statistics: http://0.0.0.0:49160/cloudinary/statistics
 - Download all resources information in CSV file: http://0.0.0.0:49160/cloudinary/csv

## Development

If you want to try it locally, in a development environment:
```bash
git clone https://gitlab.com/carlosvin/cloudinary_watcher
cd cloudinary_watcher
yarn install # or npm i
# setting environment variable with credentials and start server
CLOUDINARY_URL=cloudinary://<public key>:<here the api_secret!>@<cloud name> node server.js
```
