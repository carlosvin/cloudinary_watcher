

module.exports = class Aggregator {
    
    totalImages = 0;
    totalSize = 0;
    formats = {};
    biggestPictureSize = 0;
    smallestPictureSize = Infinity;
    biggestPicture = undefined;
    smallestPicture = undefined;

    add(images = []) {
        for (const img of images) {
            if (img.format in this.formats) {
                this.formats[img.format]++;
            } else {
                this.formats[img.format] = 1;
            }
            this.totalImages++;
            this.totalSize += img.bytes;
            if (this.biggestPictureSize < img.bytes) {
                this.biggestPicture = img.secure_url;
                this.biggestPictureSize = img.bytes;
            }
            if (this.smallestPictureSize > img.bytes) {
                this.smallestPicture = img.secure_url;
                this.smallestPictureSize = img.bytes;
            }
        }
        return this;
    }

    get avgSize () {
        if (this.totalImages > 0) {
            return Math.round(this.totalSize / this.totalImages);
        }
        return 0;
    }

    get stats () {
        return {
            totalImages: this.totalImages,
            totalSize: this.totalSize,
            formats: this.formats,
            biggestPicture: this.biggestPicture,
            smallestPicture: this.smallestPicture,
            avgSize: this.avgSize
        };
    }
}
