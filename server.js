'use strict';

const express = require('express');
const DistributedCache = require('./cache');
const Aggregator = require('./aggregator');
const Client = require('./client');
const { parse } = require('json2csv');

// Constants
const PORT = process.env.PORT || 8080;
const HOST = process.env.HOST ||'0.0.0.0';

// App
const app = express();
const aggregator = new Aggregator();
const cache = new DistributedCache();
const client = new Client(cache);

app.get('/cloudinary/statistics', async (req, res) => {
    const cached = cache.get(req.path);
    if (cached) {
        return res.json(cached);
    }
    try {
        const resources = await client.getResources();
        aggregator.add(resources);
        cache.set(req.path, aggregator.stats);
        res.json(aggregator.stats);
    } catch (e) {
        console.error(e);
        res.statusCode = 500;
        res.statusMessage = e;
        res.json(e);
    }
    
});

app.get('/cloudinary/csv', async (req, res) => {
    res.setHeader('Content-Type', 'text/csv');
    res.setHeader('Content-Disposition', `attachment; filename="resources-${Date.now()}.csv"'`);

    const cached = cache.get(req.path);
    if (cached) {
        return res.send(cached);
    }

    const resources = await client.getResources();

    if (resources && resources.length > 0) {
        try {
            const fields = [...Object.keys(Object.values(resources)[0])];
            const csv = parse(resources, { fields });
            cache.set(req.path, csv);
            res.send(csv);
        } catch (err) {
            res.statusCode = 500;
            res.send(err);
        }
    } else {
        res.end();
    }
    
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
