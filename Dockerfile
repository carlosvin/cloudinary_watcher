FROM node:13

WORKDIR /usr/src/app

COPY package*.json yarn.lock ./
RUN yarn install

# RUN npm ci --only=production

COPY . .

ENV PORT=8080
EXPOSE ${PORT}

USER node

CMD [ "node", "server.js" ]
