
const cloudinary = require('cloudinary').v2;

module.exports = class Client {
    constructor(cache) {
        this.cache = cache;
    }

    async getResources() {
        const cached = this.cache && this.cache.get('resources');
        if (cached) {
            return cached;
        }
        const resources = await this.getResourcesPage(undefined);
        this.cache && this.cache.set('resources', resources);
        return resources;
    }

    async getResourcesPage(next_cursor) {
        const result = await cloudinary.api.resources({ 
            type: 'upload', 
            max_results: 500, 
            next_cursor: next_cursor });

        if (result.next_cursor !== undefined) {
            result.resources = [...result.resources, 
                ...await this.getResourcesPage(result.next_cursor)];
        }
        return result.resources;
    }
}